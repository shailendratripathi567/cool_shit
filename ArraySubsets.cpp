    void subsets(vector<int>& nums, int l, vector<vector<int>>& ans, vector<int> res){
        if(l == 0){
            ans.push_back(res);
            return;
        }
        subsets(nums, l - 1, ans, res);
        res.push_back(nums[l - 1]);
        subsets(nums, l - 1, ans, res);
    }
    vector<vector<int>> subsets(vector<int>& nums) {
        vector<vector<int>> ans;
        vector<int> res;
        subsets(nums, nums.size(), ans, res);
        return ans;
    }
