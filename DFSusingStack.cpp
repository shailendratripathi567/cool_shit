vector<int> preorder(Node* root){
	vector<int> ans;
	if(root == NULL)
		return ans;
	stack<Node*> s;
	s.push(root);
	while(!s.empty()){
		root = s.top();
		s.pop();
		ans.push_back(root -> val);
		for(int i = root -> children.size() - 1; i >= 0; i--)
			s.push(root -> children[i]);
	}
	return ans;
}
