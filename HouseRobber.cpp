//https://leetcode.com/problems/house-robber/
    int *memo;
    int rob(vector<int>& nums, int l){
        if(l < 0)
            return 0;
        if(memo[l] >= 0)
            return memo[l];
        int result = max(rob(nums, l - 2) + nums[l], rob(nums, l - 1));
        memo[l] = result;
        return result;
    }
    int rob(vector<int>& nums) {
        memo = new int[nums.size() + 1];
        for(int i = 0; i <= nums.size(); i++)
            memo[i] = -1;
        return rob(nums, nums.size() - 1);
    }
