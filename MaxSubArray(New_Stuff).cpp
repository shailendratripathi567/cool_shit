int maxSubArray(vector<int>& nums) {
        long int max = INT_MIN;
        long int sum = INT_MIN;
        for(int i = 0; i < nums.size(); i++){
            if(nums[i] > sum + nums[i])
                sum = nums[i];
            else
                sum += nums[i];
            if(sum > max)
                max = sum;
        }
        return max;
}
