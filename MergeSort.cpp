void merge(int a[], int si, int ei){
    int mid = si + ((ei - si) / 2);
    int i = si;
    int j = mid + 1;
    int * b = new int[ei - si + 1];
    int c = 0;
    while((i <= mid) && (j <= ei)){
        if(a[i] > a[j]){
            b[c] = a[j];
            j++;
            c++;
        }
        else if(a[j] > a[i]){
            b[c] = a[i];
            i++;
            c++;
        }
        else{
            b[c] = a[i];
            i++;
            b[c + 1] = a[j];
            j++;
            c += 2;
        }
    }
    if(i <= mid)
        for(int k = i; k <= mid; k++, c++)
            b[c] = a[k];
    else if(j <= ei)
        for(int k = j; k <= ei; k++, c++)
            b[c] = a[k];
    c = 0;
    for(int k = si; k <= ei; k++, c++)
        a[k] = b[c];
    return;
}
void msort(int a[], int si, int ei){
    if(si >= ei)
        return;
    int mid = si + ((ei - si) / 2);
    msort(a, si, mid);
    msort(a, mid + 1, ei);
    merge(a, si, ei);
    return;
}
void mergeSort(int a[], int n){ //Initial Function, start from here
    msort(a, 0, n - 1);
}
