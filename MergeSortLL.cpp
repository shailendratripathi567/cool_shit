// Following is the node structure
/**************
class node{
public:
    int data;
    node * next;
    node(int data){
        this->data=data;
        this->next=NULL;
    }
};
***************/
node* merge(node *h1, node *h2) {
    node* fh = NULL;
    node* ft = NULL;
    while(h1 != NULL && h2 != NULL){
        if(h1 -> data <= h2 -> data){
            if(fh == NULL){
                fh = h1;
                ft = h1;
            }
            else{
                ft -> next = h1;
		ft = ft -> next;
            }
            h1 = h1 -> next;
        }
        else{
            if(fh == NULL){
                fh = h2;
                ft = h2;
            }
            else{
                ft -> next = h2;
		ft = ft -> next;
            }
            h2 = h2 -> next;
        }
    }
    while(h1 != NULL){
        if(fh == NULL){
            fh = h1;
            ft = h1;
        }
        else{
            ft -> next = h1;
            ft = ft -> next;
        }
        h1 = h1 -> next;
    }
    while(h2 != NULL){
        if(fh == NULL){
            fh = h2;
            ft = h2;
        }
        else{
            ft -> next = h2;
            ft = ft -> next;
        }
        h2 = h2 -> next;
    }
    /*if(h1 == NULL){
        if(fh == NULL)
            fh = h1;
        else
            ft -> next = h1;
    }
    else if(h2 == NULL){
        if(fh == NULL)
            fh = h2;
        else
            ft -> next = h2;
    }*/
    return fh;
}
node* mergeSort(node *head) {
    if(head -> next == NULL)
        return head;
    node* s = head;
    node* f = head -> next;
    while(f != NULL && f -> next != NULL){
        s = s -> next;
        f = f -> next -> next;
    }
    node* h1 = mergeSort(s -> next);
	s -> next = NULL;
    node* h2 = mergeSort(head);
    head = merge(h1, h2);
    return head;
}
