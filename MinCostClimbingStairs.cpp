    int *dp;
    int f(vector<int>& cost, int pos){
        if(pos >= cost.size())
            return INT_MAX;
        if((cost.size() - pos) <= 2)
            return cost[pos];
        if(dp[pos] != -1)
            return dp[pos];
        return dp[pos] = cost[pos] + min(f(cost, pos + 1), f(cost, pos + 2));
    }
    int minCostClimbingStairs(vector<int>& cost) {
        dp = new int[cost.size()];
        memset(dp, -1, 4 * cost.size());
        return min(f(cost, 0), f(cost, 1));
    }
