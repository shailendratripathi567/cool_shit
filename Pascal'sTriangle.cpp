vector<vector<int>> generate(int n) {
        vector<vector<int>> ans;
        if(n == 0)
            return ans;
        ans.push_back(*(new vector<int>()));
        ans[0].push_back(1);
        for(int i = 1; i < n; i++){
            vector<int> temp;
            vector<int> prev = ans[i - 1];
            temp.push_back(1);
            for(int j = 1; j < i; j++)
                temp.push_back(prev[j - 1] + prev[j]);
            temp.push_back(1);
            ans.push_back(temp);
        }
        return ans;
}
