    void quickselect(vector<pair<int, int>>& temp, int si, int ei, int k){
        if(si >= ei)
            return;
        auto pv = temp[si];
        int c = 0;
        for(int i = si + 1; i <= ei; i++)
            if(temp[i].second < pv.second)
                c++;
        swap(temp[si], temp[si + c]);
        int i = si;
        int j = ei;
        while(i < (si + c) && j > (si + c)){
            if(temp[i].second >= temp[si + c].second && temp[j].second < temp[si + c].second){
                swap(temp[i], temp[j]);
                i++;
                j--;
            }
            else if(temp[i].second >= temp[si + c].second)
                j--;
            else if(temp[j].second < temp[si + c].second)
                i++;
            else{
                i++;
                j--;
            }
        }
        int l = temp.size() - si - c;
        if(l == k)
            return;
        else if(l > k)
            quickselect(temp, si + c + 1, ei, k);
        else
            quickselect(temp, si, si + c - 1, k);
    }
    vector<int> topKFrequent(vector<int>& nums, int k) {
        unordered_map<int, int> m;
        for(int i = 0; i < nums.size(); i++)
            m[nums[i]]++;
        vector<int> res;
        /*//Using heaps and map
        priority_queue<pair<int, int>> pq;
        unordered_map<int, int>::iterator it;
        for(it = m.begin(); it != m.end(); it++){
            pq.push(make_pair(it -> second, it -> first));
            if(pq.size() > (m.size() - k)){
                res.push_back(pq.top().second);
                pq.pop();
            }
        }*/
        //Using quickselect
        vector<pair<int, int>> temp;
        for(auto it : m)
            temp.push_back(make_pair(it.first, it.second));
        quickselect(temp, 0, temp.size() - 1, k);
        int c = temp.size() - 1;
        for(int i = 0; i < k; i++, c--)
            res.push_back(temp[c].first);
        return res;
    }
