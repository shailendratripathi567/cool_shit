int part(int a[], int si, int ei){
    int i = si + 1;
    int s = 0;
    while(i <= ei){
        if(a[i] < a[si])
            s++;
        i++;
    }
    int temp = a[si];
    a[si] = a[si + s];
    a[si + s] = temp;
    i = si;
    int j = ei;
    while((i < (si + s)) && (j > (si + s))){
        if((a[i] < a[si + s]) && (a[j] >= a[si + s])){
            i++;
            j--;
        }
        else if((a[i] >= a[si + s]) && (a[j] < a[si + s])){
            temp = a[i];
            a[i] = a[j];
            a[j] = temp;
            i++;
            j--;
        }
        else if(a[i] >= a[si + s])
            j--;
        else
            i++;
    }
    return si + s;
}
void qs(int a[], int si, int ei){
    if(si >= ei)
        return;
    int pos = part(a, si, ei);
    qs(a, si, pos - 1);
    qs(a, pos + 1, ei);
    return;
}
void quickSort(int a[], int n){ //Initial Function, start from here
    qs(a, 0, n - 1);
    return;
}
