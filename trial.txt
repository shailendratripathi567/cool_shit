"git pull origin master"
To work on an up to date copy of the project

"git status"
Used when you add, change or delete files/folders

"git diff"
To view the differences between your local, unstaged changes and the repository
that you cloned or pulled

"git add <file-name or folder-name>"
To stage (prepare) a local file/folder for committing
"git add ."
For all files

"git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"
To commit (save) the staged files

"git push origin master"
To push all local commits (saved changes) to the remote repo

-> Steps to send changes to gitlab.com
1. Ex. git add trial.txt
2. git commit
3. git push origin master
